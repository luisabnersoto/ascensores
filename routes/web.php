<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 
    [SiteController::class, 'home']
); 

Route::get('llama-ascensor', 
    [SiteController::class, 'llamaAscensor']
)->name('llama_ascensor'); 

Route::get('ir-a-destino', 
    [SiteController::class, 'irADestino']
)->name('ir_a_destino'); 

Route::get('peticiones', 
    [SiteController::class, 'peticiones']
)->name('peticiones'); 

Route::get('borrar-peticiones', 
    [SiteController::class, 'borrarPeticiones']
)->name('borrar_peticiones'); 
