<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \DB;
class AscensoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! DB::table('ascensores')->count() ) {
            DB::table('ascensores')->insert([
                'name' => '1',
                'planta_actual_id'=>'1',
                'planta_actual'=>'PB',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('ascensores')->insert([
                'name' => '2',
                'planta_actual_id'=>'1',
                'planta_actual'=>'PB',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);

            DB::table('ascensores')->insert([
                'name' => '3',
                'planta_actual_id'=>'1',
                'planta_actual'=>'PB',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s")
            ]);
        }
    }
}
