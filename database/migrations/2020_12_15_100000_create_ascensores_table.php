<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAscensoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ascensores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->unsignedInteger('planta_actual_id')->nullable();
            $table->foreign('planta_actual_id')->references('id')->on('plantas')->onDelete('set null');
            $table->string('planta_actual')->nullable();

            $table->integer('plantas_recorridas')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ascensores');
    }
}
