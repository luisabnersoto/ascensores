<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeticionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peticiones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('planta_origen_id')->nullable();
            $table->foreign('planta_origen_id')->references('id')->on('plantas')->onDelete('set null');
            $table->string('planta_origen');

            $table->unsignedInteger('planta_destino_id')->nullable();
            $table->foreign('planta_destino_id')->references('id')->on('plantas')->onDelete('set null');
            $table->string('planta_destino');
            
            $table->unsignedInteger('ascensor_id')->nullable();
            $table->foreign('ascensor_id')->references('id')->on('ascensores')->onDelete('set null');

            $table->string('ascensor');
            $table->integer('plantas_recorridas')->default(0);
            $table->text('informe');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peticiones');
    }
}
