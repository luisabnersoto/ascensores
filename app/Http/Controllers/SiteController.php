<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Ascensor;
use App\Models\Planta;
use App\Models\Peticion;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function home(){
        $plantas=Planta::all();
        $peticiones= Peticion::all();

        return view('home',
            [
                'plantas'=>$plantas,
                'peticiones'=>$peticiones
        ]);
    }

    public function llamaAscensor(Request $request){
        $origen = $request->get('origen');
        $plantaOrigen = Planta::find($origen);

        if(!$plantaOrigen){
            return response()->json([]);
        }
        
        // Se busca el ascensor que esté mas cercano, y ademas que tenga menos plantas recorridas, para que el desgaste de los ascensores sea equilibrado
        $ascensor = Ascensor::select('*',\DB::raw('IF(cast(planta_actual_id as signed)-'.$origen.'>=0,cast(planta_actual_id as signed)-'
        .$origen.' ,'.$origen.'-cast(planta_actual_id as signed)) as distancia'))
        ->orderBy('distancia')
        ->orderBy('plantas_recorridas')->first();

        if(!$ascensor){
            return response()->json([]);
        }

        $peticion = new Peticion();

        $peticion->planta_origen_id=$ascensor->planta_actual_id;
        $peticion->planta_origen=$ascensor->planta_actual;
        $peticion->planta_destino_id=$plantaOrigen->id;
        $peticion->planta_destino=$plantaOrigen->name;
        
        $peticion->plantas_recorridas=$ascensor->distancia;
        $peticion->ascensor_id=$ascensor->id;
        $peticion->ascensor=$ascensor->name;

        $ascensor->plantas_recorridas+=$peticion->plantas_recorridas;
        $ascensor->planta_actual_id= $peticion->planta_destino_id;
        $ascensor->planta_actual= $peticion->planta_destino;
        $ascensor->save();

        $ascensores=Ascensor::all();
        $informe="";
        foreach ($ascensores as $key => $as) {
            $informe.="Ascensor ".$as->name." ha recorrido ".$as->plantas_recorridas
            . " plantas, actualmente se encuentra en la planta ".$as->planta_actual."<br>" ;
        }

        $peticion->informe=$informe;

        $peticion->save();

        return response()->json($ascensor);
    }

    public function irADestino(Request $request){
        $origen = $request->get('origen');
        $destino = $request->get('destino');

        $plantaOrigen = Planta::find($origen);

        if(!$plantaOrigen){
            return response()->json([]);
        }

        $plantaDestino = Planta::find($destino);

        if(!$plantaDestino){
            return response()->json([]);
        }

        $ascensor = Ascensor::select('*',\DB::raw('IF(cast(planta_actual_id as signed)-'.$destino.'>=0,cast(planta_actual_id as signed)-'
        .$destino.' ,'.$destino.'-cast(planta_actual_id as signed)) as distancia'))
        ->where('planta_actual_id',$origen)->first();
        
        if(!$ascensor){
            return response()->json([]);
        }

        $peticion = new Peticion();

        $peticion->planta_origen_id=$ascensor->planta_actual_id;
        $peticion->planta_origen=$ascensor->planta_actual;
        $peticion->planta_destino_id=$plantaDestino->id;
        $peticion->planta_destino=$plantaDestino->name;
        
        $peticion->plantas_recorridas=$ascensor->distancia;
        $peticion->ascensor_id=$ascensor->id;
        $peticion->ascensor=$ascensor->name;

        $ascensor->plantas_recorridas+=$peticion->plantas_recorridas;
        $ascensor->planta_actual_id= $peticion->planta_destino_id;
        $ascensor->planta_actual= $peticion->planta_destino;

        $ascensor->save();

        $ascensores=Ascensor::all();
        $informe="";
        foreach ($ascensores as $key => $ascensor) {
            $informe.="Ascensor ".$ascensor->name." ha recorrido ".$ascensor->plantas_recorridas
            . " plantas, actualmente se encuentra en la planta ".$ascensor->planta_actual."<br>" ;
        }

        $peticion->informe=$informe;

        $peticion->save();

        return response()->json(["status"=>"success"]);
    }

    public function peticiones(){
        return response()->json(Peticion::orderBy('id','desc')->get());
    }

    public function borrarPeticiones(){
        return response()->json(Peticion::where('id','>',0)->delete());
    }
}