<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ascensor extends Model
{
    protected $table = 'ascensores';

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}